import { 
    UPDATE_PIXEL,
    RESET_CANVAS,
    CLEAR_SWATCHES,
    ADD_SWATCH,
    SET_CANVAS_SIZE,
    SET_PIXELS_IN_SIZE,
    SELECT_CURRENT_COLOUR,
    TOGGLE_SETTINGS_VISIBILITY,
    TOGGLE_SWATCHES_VISIBILITY
} from './types';


export const updatePixel = (pixelUpdate) => {
    return ({
        type: UPDATE_PIXEL,
        payload: pixelUpdate
    });
}

export const resetCanvas = (colour, size) => {
    return {
        type: RESET_CANVAS,
        payload: { colour, size }
    }
}

export const clearSwatches = () => {
    return {
        type: CLEAR_SWATCHES
    }
}

export const addSwatch = (colour) => {
    return ({
        type: ADD_SWATCH,
        payload: colour
    })
}

export const setCanvasSize = (size) => {
    return ({
        type: SET_CANVAS_SIZE,
        payload: size
    })
}

export const setPixelsInSize = (size) => {
    return ({
        type: SET_PIXELS_IN_SIZE,
        payload: size
    })
}

export const selectCurrentColour = (colour) => {
    return ({
        type: SELECT_CURRENT_COLOUR,
        payload: colour
    })
}

export const toggleSettingsVisibility = () => {
    return ({
        type: TOGGLE_SETTINGS_VISIBILITY
    })
}

export const toggleSwatchesVisibility = () => {
    return ({
        type: TOGGLE_SWATCHES_VISIBILITY
    })
}