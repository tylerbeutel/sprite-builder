import React, { Component } from 'react';
import './Swatches.css';
import { connect } from 'react-redux';
import { toggleSwatchesVisibility } from '../../actions/index';
import Swatch from '../Swatch/Swatch';
import SwatchCreator from '../SwatchCreator/SwatchCreator';


class Swatches extends Component {
    render() {
        return (
            <div 
                className="swatches-box"  
                style={{ 
                    left: this.props.swatchesVisible ? 0 : -85,
                    opacity: this.props.swatchesVisible ? 1 : 0
                }} 
            >
                { this.props.swatches && this.props.swatches.map((colour, index) => 
                    <Swatch colour={colour} key={index} /> 
                )}
                <SwatchCreator/>
            </div>
        );
    }

}


const mapStateToProps = ({ swatches, settings }) => {
    return ({ 
        swatches,
        swatchesVisible: settings.swatchesVisible 
    });
}


export default connect(mapStateToProps, { toggleSwatchesVisibility })(Swatches);