import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pixel from '../Pixel';
import { resetCanvas } from '../../actions/index';
import './Canvas.css';



class Canvas extends Component {

    
    componentDidMount = () => {
        this.props.resetCanvas(this.props.defaultPixelColour, this.props.pixelsInSize);
    }


    render() {

        if (!this.props.canvasSize) {
            return <p>Loading...</p>
        }

        return (
            <div className="canvas" style={{ width: this.props.canvasSize }} >

                {/* Render the pixels */}
                { this.props.pixels && this.props.pixels.map((colour, index) => 
                    <Pixel data={colour} key={index} />
                )}

            </div>
        );
        
    }

}



const mapStateToProps = ({ settings, pixels }) => {
    return { 
        canvasSize: settings.canvasSize,
        pixelsInSize: settings.pixelsInSize,
        pixels,
        defaultPixelColour: settings.defaultPixelColour
    };
}



export default connect(mapStateToProps, { resetCanvas })(Canvas);