import './Settings.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setCanvasSize, setPixelsInSize, resetCanvas, clearSwatches } from '../../actions/index';



class Settings extends Component {
    state = { visible: true };


    onCanvasSizeChange = ({ target }) => {
        this.props.setCanvasSize( parseInt(target.value) );
    }


    onPixelSizeChange = ({ target }) => {
        const pixelSize = parseInt(target.value);
        this.props.setPixelsInSize(pixelSize);
        this.props.resetCanvas(this.props.defaultPixelColour, pixelSize);
    }


    render() {
        return (
            <>
                <div 
                    className="settings-box" 
                    style={{ 
                        right: this.props.settingsVisible ? 0 : -200,
                        opacity: this.props.settingsVisible ? 1 : 0
                    }} 
                >
                    <h2>Settings</h2>

                    <h3><b>Canvas:</b> {this.props.canvasSize} px</h3>
                    <input
                        type="range"
                        min={100}
                        max={500}
                        step={20}
                        value={this.props.canvasSize} 
                        onChange={ this.onCanvasSizeChange }
                    />
                    
                    
                    <h3><b>Pixels:</b> {this.props.pixelsInSize}</h3>
                    <input
                        type="range"
                        min={2}
                        max={20}
                        step={1}
                        value={this.props.pixelsInSize} 
                        onChange={ this.onPixelSizeChange }
                    />

                    <h3><b>Other Options</b></h3>
                    <button 
                        className="settings-submit"
                        onClick={() => this.props.resetCanvas(this.props.defaultPixelColour, this.props.pixelsInSize)}
                    >Reset Canvas</button>
                    <button 
                        className="settings-submit"
                        onClick={() => this.props.clearSwatches()}
                    >Clear Swatches</button>

                </div>
            </>
        );
    }
}



const mapStateToProps = ({ settings }) => {
    return ({
        canvasSize: settings.canvasSize,
        pixelsInSize: settings.pixelsInSize,
        defaultPixelColour: settings.defaultPixelColour,
        settingsVisible: settings.settingsVisible
    });
}



export default connect(mapStateToProps, { setCanvasSize, setPixelsInSize, resetCanvas, clearSwatches })(Settings);