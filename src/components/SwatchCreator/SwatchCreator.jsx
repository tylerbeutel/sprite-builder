import '../Swatch/Swatch.css';
import './SwatchCreator.css';
import { connect } from 'react-redux';
import { addSwatch } from '../../actions/index';
import React, { Component } from 'react';
import onClickOutside from 'react-onclickoutside';



class SwatchCreator extends Component {
    state = { 
        popupVisible: false,
        colourInput: '',
        currentHex: ''
    }


    componentDidMount(){
        this.hexInputRef = React.createRef();
    }


    handleClickOutside = () => {
        this.setState({ popupVisible: false })
    }


    onColourInputChange = ({ target }) => {
        // Add char is a number
        if ( !isNaN(target.Value) ) {
            this.setState({ colourInput: target.value });
        }
        // If its a valid hex character
        else if ( this.containsOnlyValidHexChars(target.value) ) {
            this.setState({ colourInput: target.value });
        }
        
        // if value is a complete and valid hex
        if ( this.isValidHex(target.value) ) {
            this.setState({ currentHex: target.value });
        }
        else {
            this.setState({ currentHex: '' });
        }
    }


    isValidHex = (value) => {
        // Check if hex has invalid length
        if (value.length !== 6) {
            console.log(value, ' is not a valid hex length:', value.length);
            return false;
        }
        // Check if value has invalid characters
        if ( !this.containsOnlyValidHexChars(value) ) {
            console.log(value, ' contains non-hex values');
            return false;
        }
        return true;
    }


    containsOnlyValidHexChars = (value) => {
        var re = new RegExp('[^0-9^a-f^A-F]+');

        if ( re.test(value) ) {
            return false;
        }
        return true;
    }


    determineBestLabelColour = () => {
        const currentHex = this.state.currentHex;

        // If no current hex value
        if (!currentHex) {
            return '000000';
        }

        // determin if colour is closer to black or white and return the opposite
        const r = parseInt( currentHex.slice(0, 2), 16 );
        const g = parseInt( currentHex.slice(2, 4), 16 );
        const b = parseInt( currentHex.slice(4, 6), 16 );
        // If closer to white
        if (r+g+b > 382) { 
            return '#000000';
        } 
        // If closer to black
        else {
            return '#ffffff';
        }

        
    }

    getSampleColour = () => {
        if (this.state.currentHex === '') {
            return '#ffffff';
        }
        return '#'+this.state.currentHex;
    }


    getLableColour = () => {
        if (this.state.currentHex === '') {
            return '#000000';
        }
        return this.determineBestLabelColour();
    }


    currentHexValue = () => {
        const colourInput = this.state.colourInput;

        if ( !this.isValidHex(colourInput) ) {
            return '#ffffff';
        }
    }


    onSwatchSubmit = (e) => {
        e.preventDefault();
        
        if (this.state.currentHex !== '') {
            this.props.addSwatch('#'+this.state.currentHex);

            let event = { target: {value:''} };
            this.onColourInputChange(event);

        }
    }


    openSwatchCreator = () => { 
        this.setState(
            { popupVisible: true }, 
            () => this.hexInputRef.current.focus()
        );
    }

 
    render() {
        return (
            <>
                <div 
                    onClick={ this.openSwatchCreator } 
                    className="swatch swatch-empty" 
                >
                    +
                    <form className={`swatch-creator-popup ${this.state.popupVisible ? 'swatch-creator-popup-visible' : ''}`}>
                        <span 
                            className="swatch-creator-inputsymbol"
                            style={{ color: this.getLableColour() }}
                        >
                            #
                        </span>
                        <input 
                            ref={this.hexInputRef}
                            type="text" 
                            className="swatch-creator-input"
                            value={ this.state.colourInput }
                            onChange={ this.onColourInputChange }
                            style={{ 
                                backgroundColor: this.getSampleColour(),
                                color: this.getLableColour()
                            }}
                            onSubmit={this.onSwatchSubmit}
                        />
    
                        <button className="swatch-creator-create" onClick={this.onSwatchSubmit}>Create</button>
                    </form>
                </div>
            </>
        );
    }
}



export default connect(null, { addSwatch })(onClickOutside(SwatchCreator));