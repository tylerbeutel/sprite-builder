import './SwatchesToggle.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleSwatchesVisibility } from '../../actions/index';


class SettingToggle extends Component {
    render() {
        return (
            <div className="icon-palette-tab" onClick={() => this.props.toggleSwatchesVisibility()} >
                <div className={`icon-palette ${this.props.swatchesVisible ? 'icon-palette-open' : ''}`} >
                    <div className="icon-splotches"></div>
                    <div className="icon-cutout"></div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = ({ settings }) => {
    return { swatchesVisible: settings.swatchesVisible };
}


export default connect(mapStateToProps, { toggleSwatchesVisibility })(SettingToggle);