import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updatePixel } from '../actions/index';


class Pixel extends Component {

    calcPixelSize = () => {
        return Math.round( (this.props.canvasSize/this.props.pixelsInSize) -0.5 );
    }

    onColourChange = () => {
        this.props.updatePixel({ 
            colour: this.props.activeColour, 
            position: this.props.data.position
        });
    }

    onMouseEnter = (e) => {
        if (e.buttons > 0) {
            this.onColourChange();
        }
    }

    render() {
        return <div 
            onMouseDown={ this.onColourChange }
            onMouseEnter={ this.onMouseEnter }
            style={{
                width: this.calcPixelSize(),
                height: this.calcPixelSize(),
                float: 'left',
                backgroundColor: this.props.data.colour,
                border: '2px solid #444444'
            }}
        />;  
    }
}  

  
const mapStateToProps = ({ settings }) => {
    return ({
        canvasSize: settings.canvasSize,
        pixelsInSize: settings.pixelsInSize,
        activeColour: settings.activeColour
    });
}


export default connect(mapStateToProps, { updatePixel })(Pixel);