import './SettingsToggle.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleSettingsVisibility } from '../../actions/index';



class SettingToggle extends Component {
    render() {
        return (
            <div className="icon-gear-tab" onClick={() => this.props.toggleSettingsVisibility()} >
                <div className={`icon-gear ${this.props.settingsVisible ? 'icon-gear-open' : ''}`} >
                    <div className="icon-edges"></div>
                    <div className="icon-hole"></div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = ({ settings }) => {
    return { settingsVisible: settings.settingsVisible };
}



export default connect(mapStateToProps, { toggleSettingsVisibility })(SettingToggle);