import React, { Component } from 'react';
import './Swatch.css';
import { connect } from 'react-redux';
import { selectCurrentColour } from '../../actions/index'



class Swatch extends Component {


    setActiveSwatch = ({ target }) => {
        const swatchValue = target.attributes.value.value;
        this.props.selectCurrentColour(swatchValue);
    }


    activeClassHandler = () => {
        if (this.props.colour === this.props.activeColour) {
            return 'swatch-active';
        }
        return '';
    }


    render() {
        return (
            <div className={`swatch ${this.activeClassHandler()}`}
                onClick={this.setActiveSwatch}
                value={this.props.colour}
                style={{
                    backgroundColor: this.props.colour
                }} 
            />
        );
    }
}



const mapStateToProps = ({ settings }) => {
    return ({ activeColour: settings.activeColour });
}



export default connect(mapStateToProps, { selectCurrentColour })(Swatch);