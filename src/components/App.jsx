import React, { Component } from 'react';
import Canvas from './Canvas/Canvas';
import Swatches from './Swatches/Swatches';
import SettingsToggle from './SettingsToggle/SettingsToggle';
import SwatchesToggle from './SwatchesToggle/SwatchesToggle';
import Settings from './Settings/Settings';



class App extends Component {

    render() {
        return (
            <div style={{
                overflow: 'hidden', 
                position: 'relative', 
                width: '100vw', 
                height: '100vh' 
            }}>
                <SettingsToggle/>
                <Settings/>
                <SwatchesToggle />
                <Swatches/>
                <Canvas />
            </div>
        );
    }

}



export default App;