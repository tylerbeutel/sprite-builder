import { combineReducers } from 'redux';
import pixelsReducer from './pixelsReducer';
import settingsReducer from './settingsReducer';
import swatchReducer from './swatchReducer';

export default combineReducers({
    swatches: swatchReducer,
    pixels: pixelsReducer,
    settings: settingsReducer
});