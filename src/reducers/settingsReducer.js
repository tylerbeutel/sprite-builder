import { 
    SELECT_CURRENT_COLOUR,
    SET_CANVAS_SIZE,
    SET_PIXELS_IN_SIZE,
    TOGGLE_SETTINGS_VISIBILITY,
    TOGGLE_SWATCHES_VISIBILITY
} from '../actions/types';


export default (state={
    canvasSize: 500,
    pixelsInSize: 5,
    currentSwatch: 0,
    defaultPixelColour: '#cccccc',
    activeColour: '#ffffff',
    settingsVisible: false,
    swatchesVisible: true
}, action) => {
    switch (action.type) {

        case SET_CANVAS_SIZE:
            return { ...state, canvasSize: action.payload };

        case SET_PIXELS_IN_SIZE:
            return { ...state, pixelsInSize: action.payload }

        case SELECT_CURRENT_COLOUR:
            return { ...state, activeColour: action.payload };

        case TOGGLE_SETTINGS_VISIBILITY:
            return { ...state, settingsVisible: !state.settingsVisible };

        case TOGGLE_SWATCHES_VISIBILITY:
            return { ...state, swatchesVisible: !state.swatchesVisible };

        default:
            return state;
            
    }
}