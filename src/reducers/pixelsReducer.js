import { 
    UPDATE_PIXEL,
    RESET_CANVAS
} from '../actions/types';


export default (state = [], action) => {
    switch (action.type) {

        case UPDATE_PIXEL:
            const index = state.findIndex(pixel => pixel.position === action.payload.position);
            return [ ...state.slice(0, index), action.payload, ...state.slice(index+1) ];

        case RESET_CANVAS:
            const size = action.payload.size * action.payload.size;
            let colours = [];
            for (let i=0; i<size; i++) {
                colours.push({ 
                    colour: action.payload.colour,
                    position: i
                });
            }
            return colours;

        default:
            console.log('default returned instead');
            return state;
    }
}
  