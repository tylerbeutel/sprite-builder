import { 
    ADD_SWATCH, 
    CLEAR_SWATCHES
} from '../actions/types';


export default (state = [
    '#000000', '#222222', '#444444', '#666666', '#888888',
    '#aaaaaa', '#cccccc', '#ffffff', '#ff0000', '#ff8000',
    '#ffff00', '#80ff00', '#00ff00', '#00ff80', '#00ffff',
    '#0080ff', '#0000ff', '#8000ff', '#ff00ff', '#ff0080' 
], action) => {
    switch (action.type) {

        case ADD_SWATCH:
            return [...state, action.payload];

        case CLEAR_SWATCHES:
            return [];

        default:
            console.log('default returned instead');
            return state;
    }
}
  